let trainer = {
	name: "Ash Ketchum",
	age: 10,
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function() {
		console.log(this.pokemon[0] + "! I choose you!");
	}
}

// Trainer
console.log(trainer);
console.log("Result of dot notation: " + trainer.name);
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["name"]);

// Talk
console.log("Result of talk method:");
trainer.talk();

// Pokemon Constructor
function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		targetHealth = target.health - this.attack;
		console.log(target.name + "'s health is now reduced to " + targetHealth);
		if(targetHealth <= 0){
			target.faint();
		}
	};
	this.faint = function(){
		console.log(this.name + " fainted!");
	}
}

// Pokemon
let pikachu = new Pokemon("Pikachu", 30);
let rattata = new Pokemon("Rattata", 8);
let mewtwo = new Pokemon("Mewtwo", 100);
let charizard = new Pokemon("Charizard", 15);
let squirtle = new Pokemon("Squirtle", 27);
let bulbasaur = new Pokemon("Bulbasaur", 29);

// Console logs: Pokemon
console.log("Pokemon contenders:");
console.log(pikachu);
console.log(rattata);
console.log(mewtwo);
console.log(charizard);
console.log(squirtle);
console.log(bulbasaur);

// PokeBattles
console.log("PokeBattles: START!")
pikachu.tackle(rattata);
mewtwo.tackle(squirtle);
charizard.tackle(bulbasaur);

